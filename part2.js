const fs = require("fs");

const data = fs
  .readFileSync("input-part2.txt")
  .toString()
  .trim()
  .split("\n")
  .map((x) => parseInt(x));

const windowSums = [];
data.forEach((x, i) => {
  let sum = 0;
  sum += x;
  sum += data[i+1] || 0;
  sum += data[i+2] || 0;
  windowSums.push(sum);
})

let prev = null;
increments = 0;
windowSums.forEach((x) => {
  if (x > prev && prev != null) {
    increments++;
  }
  prev = x;
});
console.log(increments);
