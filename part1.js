const fs = require("fs");

const data = fs
  .readFileSync("input-part1.txt")
  .toString()
  .trim()
  .split("\n")
  .map((x) => parseInt(x));

let prev = null;
increments = 0;
data.forEach((x) => {
  if (x > prev && prev != null) {
    increments++;
  }
  prev = x;
});
console.log(increments);
